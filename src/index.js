import 'babel-polyfill'

import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:tile-server:index')


import http from 'http'
import path from 'path'
import url from 'url'
import child_process from 'child_process'

import * as sharedConsts from '@kaku/common/consts'


const JPEG_QUALITY = 50


function getFilesFromUrl(reqUrl) {
  const parsedUrl = url.parse(reqUrl, true)
  if (!parsedUrl.query.pos ||
      parsedUrl.query.pos > sharedConsts.TILE_SERVER_MAX_COUNT)
    throw 'Invalid query'

  const tilesJson = Array.isArray(parsedUrl.query.pos) ?
    parsedUrl.query.pos :
    [parsedUrl.query.pos]

  const files = tilesJson
    .map(JSON.parse)
    .map((p) => path.join(process.env.KAKU_DATA_DIR, `${p[0]}_${p[1]}.png`))

  return files
}

function getGmArgs(files) {
  const args = ['convert']

  args.push(...files)
  args.push(...['-append', '-quality', JPEG_QUALITY, 'jpeg:-'])

  return args
}


function handler(req, res) {
  try {
    const files = getFilesFromUrl(req.url)
    const gmArgs = getGmArgs(files)
    const gm = child_process.spawn('gm', gmArgs)

    res.writeHead(200, {
      'Content-Type': 'image/jpeg',
      'Access-Control-Allow-Origin': '*'
    })

    gm.stdout.pipe(res)
    gm.stderr.on('data', (d) => {
      throw `GM Error: ${d.toString()}`
    })
  } catch (e) {
    debug(e)

    res.statusCode = 400
    res.end()
  }
}


http.createServer(handler).listen(
  process.env.KAKU_TILE_SERVER_PORT,
  process.env.KAKU_TILE_SERVER_ADDRESS)

debug('http listening on %s:%d',
  process.env.KAKU_TILE_SERVER_ADDRESS,
  process.env.KAKU_TILE_SERVER_PORT)
